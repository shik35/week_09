package ru.edu.task5.xml;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import ru.edu.task4.xml.MainContainer;

/**
 * ReadOnly
 */
public class AppXML {
    public static ApplicationContext run() {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("task_05.xml");
        return context;
    }
}
