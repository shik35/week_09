package ru.edu.task4.xml;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * ReadOnly
 */
public class AppXMLTest {

    @Test
    public void run() {
        assertEquals("cacheService of RealService", AppXML.run().getSomeInterface().getName());
    }
}